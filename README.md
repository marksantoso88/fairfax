# Fairfax test

To run:

1. clone repo 'git clone https://marksantoso88@bitbucket.org/marksantoso88/fairfax.git'

in terminal

1. 'cd fairfax'
2. 'yarn' to install dependencies
3. 'yarn start' to start server
4. visit http://localhost:3000 to view site.
