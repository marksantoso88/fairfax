import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { issuesFetchData, updateIssues } from '../actions/issuesActions';
import style from '../../scss/style.scss';

class IssueList extends Component {

	constructor(props) {
		super();

		this.handleSort = this.handleSort.bind(this);
		this.handleFilter = this.handleFilter.bind(this);
		this.handleRepo = this.handleRepo.bind(this);
		this.changeRepo = this.changeRepo.bind(this);

		this.state = {
			date: 'desc',
			filter: 'all',
			repo: 'facebook/react'
		}
	}

	componentWillMount() {
		this.props.fetchIssues('https://api.github.com/repos/facebook/react/issues?filter=all');
	}

	convertDate(inputFormat) {
		function pad(s) { return (s < 10) ? '0' + s : s; }
		var d = new Date(inputFormat);
		return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/');
	}

	getLabels(labels) {
		return labels.map(( label, index ) => {
			return (
				<span key={ index } className={ style.labels } style={{ backgroundColor: '#' + label.color }} > { label.name } </span>
			)
		})
	}

	sortDateAsc(a, b) {
		return new Date(a.created_at).getTime() - new Date(b.created_at).getTime();
	}

	sortDateDesc(a, b) {
		return new Date(b.created_at).getTime() - new Date(a.created_at).getTime();
	}

	handleSort(event) {

		if ( this.state.filter == 'all') {
			event.target.value == 'asc' ? this.props.list.sort(this.sortDateAsc) : this.props.list.sort(this.sortDateDesc);
		}	else {
			event.target.value == 'asc' ? this.props.filteredList.sort(this.sortDateAsc) : this.props.filteredList.sort(this.sortDateDesc);
		}

		this.setState({ date: event.target.value })
	}

	handleFilter(event) {

		if (event.target.value != 'all') {
			let obj = this.props.list.filter((issue) => {
				if( issue.state == event.target.value ) {
					return issue;
				}
			});

			this.props.updateIssues(obj);
		}

		this.setState({ filter: event.target.value });
	}

	handleRepo(event) {
 		this.setState({repo: event.target.value });
	}

	changeRepo() {
		this.props.fetchIssues(`https://api.github.com/repos/${ this.state.repo }/issues?filter=all`);
	}

	render() {

		let list = this.props.filteredList ? this.props.filteredList : this.props.list;

		if ( this.state.filter == 'all') {
			list = this.props.list;

			if ( typeof this.props.list == 'undefined' || this.props.list.length == 0) {
				list = false;
			}

		} else if ( typeof this.props.filteredList == 'undefined' || this.props.filteredList.length == 0 ) {
			list = false;
		}

		if (this.props.hasErrored) {
		  return <p>Sorry! there was an error loading the items</p>
		}

		if (this.props.isLoading) {
		  return <p>Loading github issues...</p>
		}

		return (
			<div>
				<div>
					<label>
					Change repo:
					<input value={ this.state.repo } onChange={ this.handleRepo }/> <button onClick={ this.changeRepo }>Change</button>
					</label>
					<br /><br />
					<label>Sort by Date:
						<select onChange={ this.handleSort } value={ this.state.date }>
							<option value="asc">Asc</option>
							<option value="desc">Desc</option>
						</select>
					</label>

					<label>Filter:
						<select onChange={ this.handleFilter } value={ this.state.filter }>
							<option value="open">Open</option>
							<option value="close">Closed</option>
							<option value="all">All</option>
						</select>
					</label>

				</div>
				{list ?
				<table className={ style.issues }>
				  <thead>
				  <tr>
					  <th>Author</th>
					  <th>Title</th>
					  <th>Date</th>
					  <th>Tags</th>
				  </tr>
				  </thead>
				  <tbody>
					  { list.map((issue) => {
						  const date = this.convertDate(issue.created_at);
						  const labels = this.getLabels(issue.labels);

						  return (
							<tr key={ issue.id }>
							  <td className={ style.title }>
								  <img src={ issue.user.avatar_url } width="50" height="50" />
								  <div>{ issue.user.login }</div>
							  </td>
							  <td>
								  <a href={ issue.html_url } >
									  { issue.title }
								  </a>
							  </td>
							  <td>
								  { date }
							  </td>
							  <td >
								  { labels }
							  </td>
							</tr>
						  )
					  }) }
				  </tbody>
				</table>
				:
				<span className={ style.noResults }>Sorry no results found</span>
				}
			</div>
		);
	}
}

IssueList.PropTypes = {
  fetchData: PropTypes.func.isRequired,
};

const mapStateToProps = ({ issues }) => {
	const { list, filteredList, hasErrored, isLoading } = issues;
	return { list, filteredList, hasErrored, isLoading }
};

const mapDispatchToProps = (dispatch) => ({
  fetchIssues: (url) => dispatch(issuesFetchData(url)),
  updateIssues: (issues) => dispatch(updateIssues(issues))
});

export default connect(mapStateToProps, mapDispatchToProps)(IssueList);
