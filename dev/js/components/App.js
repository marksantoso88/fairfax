import React, { Component } from 'react';
import IssueList from './IssueList';

export default class App extends Component {
  render() {
    return (
      <div>
        <IssueList />
      </div>
    );
  }
}
