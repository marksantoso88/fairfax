import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';
import allReducers from '../reducers';
import promise from 'redux-promise';

const logger = createLogger();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

 

export default function configureStore(initialState) {
  return createStore(
    allReducers,
    initialState,
    composeEnhancers(applyMiddleware(thunk, logger, promise))
  );
}
