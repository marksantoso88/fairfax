const INITIAL_STATE = { hasErrored: false, isLoading: true};

export default function(state = INITIAL_STATE, action) {
	 switch(action.type) {
		case 'ISSUES_HAS_ERRORED':
		 	return { ...state, ...{ hasErrored: action.hasErrored }};
		case 'ISSUES_IS_LOADING':
		 	return { ...state, ...{ isLoading: action.isLoading }};
		case 'ISSUES_FETCH_DATA_SUCCESS':
			return { ...state, ...{ list: action.list } }
		case 'UPDATE_ISSUES':
			return { ...state, ...{ filteredList: action.filteredList } }
		default:
			return state;
	}
}
