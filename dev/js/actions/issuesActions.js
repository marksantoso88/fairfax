export function issuesHasErrored(bool) {
  return {
	type: 'ISSUES_HAS_ERRORED',
	hasErrored: bool
  }
}

export function issuesIsLoading(bool) {
  return {
	type: 'ISSUES_IS_LOADING',
	isLoading: bool
  }
}

export function issuesFetchDataSuccess(issues) {

   return {
	type: 'ISSUES_FETCH_DATA_SUCCESS',
	list: issues
  }
}

export function updateIssues(issues) {

  return {
	type: 'UPDATE_ISSUES',
	filteredList: issues
  }
}


export function issuesFetchData(url) {
  return (dispatch) => {

	dispatch(issuesIsLoading(true));

	var request = new Request(url, {
	    body: { filter: 'all' }
	});

	fetch(request)
	  .then((response) => {
		  if (!response.ok) {
			  throw Error(response.statusText);
		  }
		  dispatch(issuesIsLoading(false));
		  return response;
	  })
	  .then((response) => response.json())
	  .then((issues) => {
 		  dispatch(issuesFetchDataSuccess(issues))
	  })
	  .catch(() => dispatch(issuesHasErrored(true)));
  };
}
